.TH gfs2_edit 8

.SH NAME
gfs2_edit - Display, print or edit GFS2 or GFS internal structures.

.SH SYNOPSIS
.B gfs2_edit
[\fIOPTION\fR]... [\fIDEVICE\fR]

.SH DESCRIPTION
The gfs2_edit command is a tool used to examine, edit or
display internal data structures of a GFS2 or GFS file system.
The gfs2_edit command can be run interactively, as described
below in INTERACTIVE MODE.

Caution: Several options of the gfs2_edit command alter the
file system metadata and can cause file system corruption.
These options should be used with great care.

.SH OPTIONS
.TP
\fB-p\fP [\fIstructure\fR | \fIblock\fR]
Print a gfs2 data structure in human-readable format to stdout.
You can enter either a block number or a data structure name.  Block numbers
may be specified in hex (e.g., 0x10) or decimal (e.g., 16).

You can specify the following data structure names with the -p option.

\fIsb\fR, \fIsuperblock\fR - Print the superblock.

\fIroot\fR - Print the root directory.

\fImaster\fR - Print the master system directory.

\fIjindex\fR - Print the journal index system directory.

\fIper_node\fR - Print the per_node system directory.

\fIinum\fR - Print the system inum file.

\fIstatfs\fR - Print the system statfs file.

\fIrindex\fR, \fIrgindex\fR - Print the resource group index system file.

\fIrg X\fR - Print the resource group information for RG X (zero-based).

\fIrgs\fR - Print the resource group information.

\fIquota\fR - Print the contents of the system quota file.

\fIidentify\fR - Identify a data block rather than print the block's contents.

\fIsize\fR - Print the device size information.

\fIjournalX\fR - Print the contents of journal X, where X is a journal
number from 0 to <the number of journals in your file system - 1>.
Only the journal headers and journal descriptors are dumped.  For journal
descriptors, this option prints out every file system block number logged
in that section of the journal.  The actual journaled blocks are not printed.

If you specify a block number rather than a structure name, gfs2_edit will
print out a breakdown of the structure for that block.
For example: \fBgfs2_edit -p sb\fP will print the superblock, but so does
\fBgfs2_edit -p 0x10\fP and \fBgfs2_edit -p 16\fP.

If you specify -p without a block or structure name, gfs2_edit prints the
superblock.

You can specify more than one data structure with a single -p option.
For example, "gfs2_edit -p inum statfs /dev/sda1" prints the system inum
file and the system statfs file on /dev/sda1.

.TP
\fB-s\fP [\fIstructure\fR | \fIblock\fR]
Specify a starting block for interactive mode.  Any of the keywords found
in the -p option may be specified.  If you want to start on a particular
resource group, specify it in quotes.  For example, gfs2_edit -s "rg 3"
.TP
\fB-h, -help, -usage\fP
Print help information.
.TP
\fB-c\fP [\fI0\fR | \fI1\fR]
Use alternate color scheme for interactive mode: 0=normal (dark colors on
white background), or 1 (light colors on black background).
.TP
\fB-V\fP
Print program version information only.
.TP
\fB-x\fP
Print in hex mode.

.TP
\fBrg\fP \fI<rg>\fR \fI<device>\fR
Print the contents of Resource Group \fI<rg>\fR on \fI<device>\fR.

\fI<rg>\fR is a number from 0 to X - 1, where X is the number of RGs.
.TP
\fBrgcount\fP \fI<device>\fR
Print the number of Resource Groups in the file system on \fI<device>\fR.
.TP
\fBrgflags\fP \fI<rg>\fR [\fInew_value\fR] \fI<device>\fR
Print and/or modify the rg_flags value of Resource Group \fI<rg>\fR on
\fI<device>\fR.

\fI<rg>\fR is a number from 0 to X - 1, where X is the number of RGs.
If \fInew_value\fR is not specified, the current rg_flags value will be
printed but not modified.  If \fInew_value\fR is specified, the rg_flags
field will be overwritten with the new value.
.TP
\fBprintsavedmeta\fP \fI<filename>\fR
Print off a list of blocks from <filename> that were saved with the savemeta
option.
.TP
\fBsavemeta\fP \fI<device>\fR \fI<filename>\fR
Save off the GFS2 metadata (not user data) for the file system on the
specified device to a file given by <filename>.  You can use this option
to analyze file system problems without revealing sensitive information
that may be contained in the files.  This option works quickly by
using the system bitmap blocks in the resource groups to determine the
location of all the metadata.  If there is corruption
in the bitmaps, resource groups or rindex file, this method may fail and
you may need to use the savemetaslow option.
The destination file is not compressed.  You may want to compress it
with a program such as bzip2 before sending it for analysis.
.TP
\fBsavemetaslow\fP \fI<device>\fR \fI<filename>\fR
Save off GFS2 metadata, as with the savemeta option, examining every
block in the file system for metadata.  This option is less prone to failure
due to file system corruption than the savemeta option, but it is 
extremely slow.
.TP
\fBsavergs\fP \fI<device>\fR \fI<filename>\fR
Save off only the GFS2 resource group metadata for the file system on the
specified device to a file given by <filename>.
.TP
\fBrestoremeta\fP \fI<filename>\fR \fI<dest device>\fR
Take a file created with the savemeta option and restores its
contents on top of the specified destination device.  \fBWARNING\fP:
When you use this option, the file system and all data on the 
destination device is destroyed.  Since only metadata (but no data) 
is restored, every file in the resulting file system is likely to be
corrupt.  The ONLY purpose of this option is to examine and debug file
system problems by restoring and examining the state of the saved metadata.
If the destination file system is the same size or larger than the source
file system where the metadata was saved, the resulting file system
will be the same size as the source.  If the destination device is
smaller than the source file system, gfs2_edit will restore as much as
it can, then quit, leaving you with a file system that probably will not
mount, but from which you might still be able to figure out what is
wrong with the source file system.

.SH INTERACTIVE MODE
If you specify a device on the gfs2_edit command line and you specify
no options other than -c, gfs2_edit will act as an interactive GFS2
file system editor for the file system you specify.  There
are three display modes: hex mode, structure mode and pointers mode.
You use the m key to switch between the modes, as described below.
The modes are as follows:
.TP
Hex mode (default)
Display or edit blocks of the file system in hexadecimal and ascii.

Lines at the top indicate the currently displayed block in both hex and
decimal.  If the block contains a GFS2 data structure, the name of that
structure will appear in the upper right corner of the display.
If the block is a well-known block, such as the superblock or rindex,
there will be a line to indicate what it is.

In hex mode, you can edit blocks by pressing \fB<enter>\fP and entering
hexadecimal digits to replace the highlighted hex digits.  Do NOT precede
the numbers with "0x".  For example, if you want to change the value at
offset 0x60 from a 0x12 to 0xef, position your cursor to offset 0x60,
so that the 12 is highlighted, then press \fB<enter>\fP and type in "ef".
Press \fB<escape>\fP or \fB<enter>\fP to exit edit mode.

In hex mode, different colors indicate different things.
For example, in the default color scheme, the GFS2 data structure will
be black, data offsets will be light blue, and actual data (anything after
the gfs2 data structure) will be red.

.TP
Structure mode
Decode the file system block into its GFS2 structure and
display the values of that structure.  This mode is most useful for
jumping around the file system.  For example, you can use the arrow 
keys to position down to a pointer and press \fBJ\fP to jump to that block.

.TP
Pointers mode
Display any additional information appearing on the block.
For example, if an inode has block pointers, this will display them and
allow you to scroll through them.  You can also position to one of them
and press \fBJ\fP to jump to that block.

.SH Interactive mode command keys:
.TP
\fBq\fP or \fB<esc>\fP
The \fBq\fP or \fB<escape>\fP keys are used to exit gfs2_edit.

.TP
\fB<arrow/movement keys>\fP up, down, right, left, pg-up, pg-down, home, end
The arrow keys are used to highlight an area of the display.  The \fBJ\fP
key may be used to jump to the block that is highlighted.

.TP
\fBm\fP - Mode switch
The \fBm\fP key is used to switch between the three display modes.
The initial mode is hex mode.  Pressing the \fBm\fP key once switches to
structure mode.  Pressing it a second time switches from structure mode
to pointers mode.  Pressing it a third time takes you back to hex mode again.

.TP
\fBj\fP - Jump to block
The \fBj\fP key jumps to the block number that is currently highlighted.
In hex mode, hitting J will work when any byte of the pointer is highlighted.

.TP
\fBg\fP - Goto block
The \fBg\fP key asks for a block number, then jumps there.  Note that
in many cases, you can also arrow up so that the current block number
is highlighted, then press \fB<enter>\fP to enter a block number to jump to.

.TP
\fBh\fP - Help display
The \fBh\fP key causes the interactive help display to be shown.

.TP
\fBe\fP - Extended mode
The \fBe\fP key causes gfs2_edit to switch to extended ("pointers") mode.

.TP
\fBc\fP - Color scheme
The \fBc\fP key causes gfs2_edit to switch to its alternate color scheme.

.TP
\fBf\fP - Forward block
The \fBf\fP key causes you to scroll forward one block.  This does
not affect the "jump" status.  In other words, if you use the \fBf\fP
key to move forward several blocks, pressing \fB<backspace>\fP will
not roll you back up.

.TP
\fB<enter>\fP - Edit value
The \fB<enter>\fP key causes you to go from display mode to edit mode.
If you are in hex mode and you hit enter, you can type new hex values
at the cursor's current location.  Note: Currently hitting \fB<enter>\fP
in structure mode allows you to enter a new value, but it will not actually
change the value on disk.  That is a future feature.

.TP
\fB<home>\fP
If you are in pointers mode, this takes you back to the starts of the
pointers you are viewing.  Otherwise it takes you back to the superblock.

.TP
\fB<backspace>\fP
This takes you back to the block you were displaying before a jump.

.TP
\fB<space>\fP
This takes you forward to the block you were displaying when you hit
\fB<backspace>\fP.

.SH EXAMPLES
.TP
gfs2_edit /dev/roth_vg/roth_lv
Display and optionally edit the file system on /dev/roth_vg/roth_lv

.TP
gfs2_edit -p sb /dev/vg0/lvol0
Print the superblock of the gfs2 file system located on
/dev/vg0/lvol0.

.TP
gfs2_edit -p identify 2746 2748 /dev/sda2
Print out what kind of blocks are at block numbers 2746 and 2748 on
device /dev/sda2.

.TP
gfs2_edit -p rindex /dev/sda1
Print the resource group index system file located on device
/dev/sda1.

.TP
gfs2_edit savemeta /dev/sda1 /tmp/our_fs
Save off all metadata (but no user data) to file /tmp/our_fs.

.TP
gfs2_edit -p root /dev/my_vg/my_lv
Print the contents of the root directory in /dev/my_vg/my_lv.

.TP
gfs2-edit -x -p 0x3f7a /dev/sda1
Print the contents of block 16250 of /dev/sda1 in hex.

.TP
gfs2_edit -p 12345 /dev/sdc2
Print the gfs2 data structure at block 12345.

.TP
gfs2_edit rgcount /dev/sdb1
Print how many Resource Groups exist for /dev/sdb1.

.TP
gfs2_edit -p rg 17 /dev/sdb1
Print the contents of the eighteenth Resource Group on /dev/sdb1.

.TP
gfs2_edit rgflags 3 /dev/sdb1
Print the rg_flags value for the fourth Resource Group on /dev/sdb1.

.TP
gfs2_edit rgflags 3 8 /dev/sdb1
Set the GFS2_RGF_NOALLOC flag on for the fourth Resource Group on /dev/sdb1.

.SH KNOWN BUGS
.TP
The directory code does not work well.  It might be confused
by directory "sentinel" entries.
