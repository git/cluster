#ifndef __CNX_MGR_DOT_H__
#define __CNX_MGR_DOT_H__

int process_request(int afd);
int process_broadcast(int sfd);

#endif /* __CNX_MGR_DOT_H__ */
