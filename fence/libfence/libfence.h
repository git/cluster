#ifndef _LIBFENCE_H_
#define _LIBFENCE_H_

#ifdef __cplusplus
extern "C" {
#endif

int fence_node(char *name);

#ifdef __cplusplus
}
#endif

#endif
